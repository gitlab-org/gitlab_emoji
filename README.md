# GitLab Emoji

Gem extending [gemojione gem](https://github.com/jonathanwiesel/gemojione).

# Assets Precompiling

```
# config/application.rb
config.assets.paths << Gemojione.index.images_path
config.assets.precompile << "emoji/*.png"
```
